/*
 program takes 2 arguments: date and location, in that order. pass parameters as strings only.
 at the time, location is hardcoded for brno and bratislava. if you are feeling adventurous,
 uncomment the relevant parts and input location as coordinates in XX.XXN/S,YY.YYE/W format.
 be aware though, absolutely NO input sanitization or checking is present at the time, use with caution!
 the program should crash cleanly if an error occurs but no guarantees.
 The api used in this program is described in detail here: https://aa.usno.navy.mil/data/docs/api.php
*/

import Foundation

var args:[String] = []

if CommandLine.arguments.count < 2 {
    args.append("default")
}
else {
    CommandLine.arguments.forEach {
        args.append($0)
    }
    args.remove(at: 0)
}

var COORDS: String? = nil
var TZ: String = "2" //timezone: hardcoded for now to CEST
var DATE: String? = nil
var url = URL(string:"")
var quit: Bool = false
var gtg: Bool = false
var dfltSet: Bool = false
var errInInp: Bool = false

if args.count > 2 {
    print("Invalid parameter count!")
    errInInp = true
}

switch args[0] {
case "default" :
    COORDS = "49.19N,16.60E" //Brno
    DATE = "today"
    gtg = true
    dfltSet = true
    print("No location or date provided, using today's date. Data for Brno")
case "yesterday":
    DATE = "yesterday"
case "today" :
    DATE = "today"
case "tomorrow":
    DATE = "tomorrow"
default:
    if !errInInp {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MM/DD/YYYY" //Api only accepts date in this format, sorry
        let argDate = args[0]
        if dateFormatterGet.date(from: argDate) != nil {
            DATE = args[0]
        } else {
            DATE = "today"
            print("Invalid date format, proceeding with today's date (input date as MM/DD/YYYY or today, tomorrow, yesterday)")
        }
    }
}

if args.count > 1 {
    switch args[1] {
    case "Brno" :
        COORDS = "49.19N,16.60E" //Brno
        gtg = true
    case "brno":
        COORDS = "49.19N,16.60E" //Brno
        gtg = true
    case "Bratislava":
        COORDS = "48.15N,17.09E" //Bratislava
        gtg = true
    case "bratislava":
        COORDS = "48.15N,17.09E" //Bratislava
        gtg = true
    default:
        if !errInInp {
            COORDS = "49.19N,16.60E" //Brno
            print("Invalid location argument, data for Brno")
            gtg = true
            //comment out the above and uncomment this for coordinate input as unsanitized launch parameter
            /*
            COORDS = args[1] //pass location argument as coordinates in XX.XXN/S,YY.YYE/W format. no correctness check, be careful!
            gtg = true
            */
        }
    }
}

if args.count < 2 {
    if !dfltSet {
        if !errInInp {
            COORDS = "49.19N,16.60E" //Brno
            gtg = true
            print("No location provided, data for Brno")
        }
    }
}

if errInInp {
    quit = true
}
else if DATE != nil && COORDS != nil && gtg == true {
    url = URL(string: "http://api.usno.navy.mil/rstt/oneday?date=\(String(DATE!))&coords=\(String(COORDS!))&tz=\(TZ)&ID=dpt")
    
    func getJsonFromUrl() {
        let dlTask = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
                //print(jsonObj!.value(forKey: "apiversion")!)
                let astroData = AstroData()
                astroData.fetch(apiData: jsonObj!)
                astroData.present()
            }
            quit = true
        }
        dlTask.resume()
    }
    getJsonFromUrl()
}
else {
    print("Missing or invalid parameters!")
    quit = true
}

while !quit {
    sleep(1)
}
