
import Foundation


protocol Presentable {
    func present()
}
