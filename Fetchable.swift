
import Foundation

protocol Fetchable {
    func fetch(apiData: NSDictionary)
}
