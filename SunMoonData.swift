
import Foundation

class SunMoonData: Presentable, Fetchable {
    var riseTime: String?
    var upperTransitTime: String?
    var setTime: String?
    
    func present() {
        // overriden
    }
    
    func fetch(apiData: NSDictionary) {
        
    }
}
