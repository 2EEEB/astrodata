
import Foundation

class MoonData: SunMoonData {
    override func present() {
        print("---- Moon Data ----")
        if riseTime != nil {
            print("The moon will rise (rose) at \(String(riseTime!))")
        }
        if upperTransitTime != nil {
            print("The moon will reach (reached) its highest point at \(String(upperTransitTime!))")
        }
        if setTime != nil {
            print("The moon will set (set) at \(String(setTime!))")
        }
    }
    override func fetch(apiData: NSDictionary) {
        let tmpArry = apiData.value(forKey: "moondata") as? [NSDictionary]
        for dict in tmpArry! {
            switch dict.value(forKey: "phen") as? String {
            case "R": self.riseTime = dict.value(forKey: "time") as? String
            case "U": self.upperTransitTime = dict.value(forKey: "time") as? String
            case "S": self.setTime = dict.value(forKey: "time") as? String
            default: break
            }
        }
    }
}
