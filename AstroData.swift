
import Foundation

class AstroData: Presentable, Fetchable {
    var error: String?
    var year: Int?
    var month: Int?
    var day: Int?
    var dayOfWeek: String?
    var sunData: SunData?
    var moonData: MoonData?
    var closestPhaseName: String?
    var closestPhaseDate: String?
    var closestPhaseTime: String?
    var fracillum: String?
    var curPhase: String?
    
    func present() {
        print("--------Astronomical Data--------")
        if year != nil && month != nil && day != nil && dayOfWeek != nil {
            print("Requested date is \(String(day!))/\(String(month!))/\(String(year!)) which is (was/will be) a \(String(dayOfWeek!))")
        }
        else {
            print("o fug")
        }
        if self.sunData != nil {
            self.sunData!.present()
        }
        if self.moonData != nil {
            self.moonData!.present()
        }
        if fracillum != nil {
            print("Illumination of the moon is (was/will be) \(String(fracillum!))")
        }
        if curPhase != nil {
            print("The moon is (was/will be) in its \(String(curPhase!)) phase.")
        }
        if closestPhaseName != nil && closestPhaseDate != nil && closestPhaseTime != nil {
            print("Closest moon phase is (was/will be) \(String(closestPhaseName!)), occuring on \(String(closestPhaseDate!)) at \(String(closestPhaseTime!))")
        }
    }
    
    func fetch(apiData: NSDictionary) {
        self.error = apiData.value(forKey: "error") as? String
        self.year = apiData.value(forKey: "year") as? Int
        self.month = apiData.value(forKey: "month") as? Int
        self.day = apiData.value(forKey: "day") as? Int
        self.dayOfWeek = apiData.value(forKey: "dayofweek") as? String
        self.fracillum = apiData.value(forKey: "fracillum") as? String
        self.curPhase = apiData.value(forKey: "curphase") as? String
        let tmpDict = apiData.value(forKey: "closestphase") as? NSDictionary
        self.closestPhaseName = tmpDict?.value(forKey: "phase") as? String
        self.closestPhaseDate = tmpDict?.value(forKey: "date") as? String
        self.closestPhaseTime = tmpDict?.value(forKey: "time") as? String
        self.sunData = SunData()
        self.sunData!.fetch(apiData: apiData)
        self.moonData = MoonData()
        self.moonData!.fetch(apiData: apiData)
    }
}
