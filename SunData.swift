
import Foundation

class SunData: SunMoonData {
    var beginCivilTwilight: String?
    var endCivilTwilight: String?
    
    override func present() {
        print("---- Sun Data ----")
        if beginCivilTwilight != nil {
            print("First light will be (was) visible at \(String(beginCivilTwilight!))")
        }
        if riseTime != nil {
            print("The sun will rise (rose) at \(String(riseTime!))")
        }
        if upperTransitTime != nil {
            print("The sun will reach (reached) its highest point at \(String(upperTransitTime!))")
        }
        if setTime != nil {
            print("The sun will set (set) at \(String(setTime!))")
        }
        if endCivilTwilight != nil{
            print("It will be (was) dark at \(String(endCivilTwilight!))")
        }
    }
    
    override func fetch(apiData: NSDictionary) {
        let tmpArry = apiData.value(forKey: "sundata") as? [NSDictionary]
        for dict in tmpArry! {
            switch dict.value(forKey: "phen") as? String {
            case "R": self.riseTime = dict.value(forKey: "time") as? String
            case "U": self.upperTransitTime = dict.value(forKey: "time") as? String
            case "S": self.setTime = dict.value(forKey: "time") as? String
            case "BC": self.beginCivilTwilight = dict.value(forKey: "time") as? String
            case "EC": self.endCivilTwilight = dict.value(forKey: "time") as? String
            default: break
            }
        }
    }
}
